/*
 * skipcontainer.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "skipcontainer.h"
#include <QtCore/QByteArray>


SkipContainer::SkipContainer()
{
}

bool SkipContainer::init(int scenarioCount,const string &homeDirectory)
{
	bool ret = true;
	m_sceneVector.reserve(scenarioCount);
	for(int i = 0;i < scenarioCount;i++)
		m_sceneVector.push_back(0);

	string skipSaveString = homeDirectory;
	skipSaveString += "/skip.sav";
	m_skipSaveFile.setFileName(skipSaveString.c_str());
	if(!m_skipSaveFile.open(QIODevice::ReadWrite))
	{
		ret = false;
		fprintf(stderr,"error, could not open file: %s\n",skipSaveString.c_str());
	}
	if(ret)
	{
		if(m_skipSaveFile.seek(0))
		{
			QByteArray skipData = m_skipSaveFile.readAll();
			if(skipData.length() >= (int) sizeof(int))
			{
				int count = *((int*) skipData.data());
				for(int i = 0;i < count;i++)
					m_sceneVector.at(i) = *((int*) (skipData.data()) + 1 + i);
			}
		}
		else
			fprintf(stderr,"error, could not seek file: %s\n",m_skipSaveFile.fileName().toStdString().c_str());
	}
	return ret;
}

void SkipContainer::update(int scenario,int scene)
{
	if(scene > m_sceneVector.at(scenario))
	{
		m_sceneVector.at(scenario) = scene;
		int *fileData = new int[m_sceneVector.size() + 1];
		fileData[0] = (int) m_sceneVector.size();
		for(int i = 0;i < (int) m_sceneVector.size();i++)
			fileData[i + 1] = m_sceneVector.at(i);
		if(m_skipSaveFile.seek(0))
		{
			if(m_skipSaveFile.write((char*) fileData,(m_sceneVector.size() + 1) * sizeof(int)) != (int) ((m_sceneVector.size() + 1) * sizeof(int)))
				fprintf(stderr,"error, write failed in file: %s\n",m_skipSaveFile.fileName().toStdString().c_str());
		}
		else
			fprintf(stderr,"error, could not seek file: %s\n",m_skipSaveFile.fileName().toStdString().c_str());
		delete[] fileData;
	}
}

bool SkipContainer::isSkipAvailable(int scenario,int scene)
{
	bool ret = false;
	if(scene < m_sceneVector.at(scenario))
		ret = true;
	return ret;
}
