/*
 * gameplayvideowidget.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "gameplayvideowidget.h"
#include "mainwindow.h"

GamePlayVideoWidget::GamePlayVideoWidget(QWidget *parent /*= 0*/) : Phonon::VideoWidget(parent)
{
	m_mainWindow = (MainWindow*) parent;
}

void GamePlayVideoWidget::mousePressEvent(QMouseEvent *event)
{
	if(m_mainWindow->isSkipping())
		m_mainWindow->unsetSkipping();
	else
	{
		if(m_mainWindow->getCurrentSceneType() == GameScene::SCENE_MOVIE)
		{
			if(event->button() == Qt::LeftButton)
				m_mainWindow->accelerateMovie();
			else
				QWidget::mousePressEvent(event);
		}
		else
			QWidget::mousePressEvent(event);
	}
}

