/*
 * skipcontainer.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SKIPCONTAINER_H
#define SKIPCONTAINER_H

#include <QtCore/QFile>
#include <vector>
#include <string>

using namespace std;

class SkipContainer
{
public:
    SkipContainer();
private:
	SkipContainer(const SkipContainer&) {;}
	SkipContainer &operator = (const SkipContainer&) {return *this;}
public:
	bool init(int scenarioCount,const string &homeDirectory);

	void update(int scenario,int scene);
	bool isSkipAvailable(int scenario,int scene);
private:
	QFile m_skipSaveFile;

	vector<int> m_sceneVector;
};

#endif // SKIPCONTAINER_H
