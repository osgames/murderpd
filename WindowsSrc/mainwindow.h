/*
 * mainwindow.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "builddefs.h"
#include <QtGui/QMainWindow>
#include <QtGui/QVBoxLayout>
#include <QtGui/QPushButton>
#include <Phonon/MediaObject>
#include <Phonon/AudioOutput>
#include <QtGui/QFrame>
#include <QtGui/QPixmap>
#include <QtGui/QLabel>
#include <QtGui/QStackedLayout>
#include <QtGui/QPainter>
#include <QtGui/QPicture>
#include <QtCore/QTimer>
#include <QtCore/QRect>
#include <QtCore/QXmlStreamReader>
#include <QtGui/QSpinBox>
#include <QtCore/QBuffer>
#include <QtGui/QScrollArea>
#include <QtGui/QTextEdit>
#include <QtCore/QMutex>
#include <QtGui/QTextBrowser>
#include <vector>
#include <string>
#include "gameplaylabel.h"
#include "gamescenario.h"
#include "skipcontainer.h"
#include "saveloadcontainer.h"
#include "audiovideomonitorthread.h"
#include "gameplayvideowidget.h"

using namespace std;

#define APPLICATION_TITLE						"A Murder In The Public Domain"
#define APPLICATION_NAME						"MurderPD"
#define ORGANIZATION_NAME						"Melon"
#define APPLICATION_VERSION						"1.3.1"

#define IMAGE_MOVIE_MINIMUM_WIDTH				640
#define IMAGE_MOVIE_MINIMUM_HEIGHT				480

#define CONVERSATION_MINIMUM_HEIGHT				128

#define DEFAULT_TOP_SPRITE						20
#define DEFAULT_MIDDLE_SPRITE					150
#define DEFAULT_BOTTOM_SPRITE					300

#define DEFAULT_SPRITE_HEIGHT					160

#define SPRITE_SLIDE_X_STEP						40
#define SPRITE_SLIDE_Y_STEP						30

#define SPRITE_SLIDE_TIMER_TIME					60

#define WRITE_CONVERSATION_TIMER_TIME			40
#define WRITE_CONVERSATION_TIMER_SKIP_TIME		200

#define DEFAULT_TITLE_BUTTON_WIDTH				240

#define TITLE_PIXMAP_DISPLAY_WIDTH				432
#define TITLE_PIXMAP_DISPLAY_HEIGHT				324

#define SAVE_LOAD_PIXMAP_WIDTH					160
#define SAVE_LOAD_PIXMAP_HEIGHT					120

#define DEFAULT_SAVE_LOAD_BUTTON_WIDTH			160

#define SAVE_LOAD_MAX_PAGE						3

#define GAME_PLAY_BUTTON_WIDTH					100

#define WINDOW_TOP_POSITION						64

#define CONVERSATION_FONT_SIZE					12
#define TITLE_FONT_SIZE							16

#define CONVERSATION_MARGIN						16
#define CONVERASTION_LINE_WIDTH					4

#define FRAME_LINE_WIDTH						4

#define MONITOR_THREAD_DESTRUCTOR_WAIT			1000

#define SAVE_LOAD_SPACING						32

#define ABOUT_ICON_WIDTH						64
#define ABOUT_ICON_HEIGHT						64

#define MOVIE_TICK_INTERVAL						100
#define SOUND_EFFECT_TICK_INTERVAL				100
#define CONVERSATION_TYPING_TICK_INTERVAL		100


class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	enum
	{
		PAGE_TITLE,
		PAGE_GAME
	};

	enum
	{
		SPRITE_VERTICAL_TOP,
		SPRITE_VERTICAL_MIDDLE,
		SPRITE_VERTICAL_BOTTOM
	};

	enum
	{
		SPRITE_HORIZONTAL_OFF_SCREEN,
		SPRITE_HORIZONTAL_LEFT,
		SPRITE_HORIZONTAL_MIDDLE,
		SPRITE_HORIZONTAL_RIGHT
	};

	enum
	{
		SPRITE_DIRECTION_FROM_LEFT,
		SPRITE_DIRECTION_FROM_RIGHT
	};

	// these are the background images, they should be 640x480 png
	// enums can be in any order, i alphabetize them for convenience while creating distributions
	// when adding a background you must modify the methods loadBackgrounds, getBackgroundOffsetFromText, and getBackgroundTextFromOffset
	enum
	{
		BACKGROUND_BAR,
		BACKGROUND_BEACH,
		BACKGROUND_BLACK,
		BACKGROUND_BRIDGE,
		BACKGROUND_CAVE,
		BACKGROUND_CEMETERY,
		BACKGROUND_CITY,
		BACKGROUND_CLOUDS,
		BACKGROUND_DESERT,
		BACKGROUND_DINER,
		BACKGROUND_DINER_INSIDE,
		BACKGROUND_DON_OFFICE,
		BACKGROUND_FORREST,
		BACKGROUND_GAME_OVER,
		BACKGROUND_HOTEL_BASEMENT,
		BACKGROUND_HOTEL_DINING_ROOM,
		BACKGROUND_INSIDE_TRAIN,
		BACKGROUND_INSIDE_TRAIN_2,
		BACKGROUND_JAIL,
		BACKGROUND_LEEDS_HOUSE,
		BACKGROUND_LEEDS_INSIDE,
		BACKGROUND_LOBBY,
		BACKGROUND_LONELY_GRAVE,
		BACKGROUND_MARY_HOTEL_ROOM,
		BACKGROUND_MONA_BEDROOM,
		BACKGROUND_MONA_LOBBY,
		BACKGROUND_MONIQUE_BEDROOM,
		BACKGROUND_MOON,
		BACKGROUND_NECTAR,
		BACKGROUND_NORTH_STORM,
		BACKGROUND_OFFICE,
		BACKGROUND_OLD_ROME,
		BACKGROUND_ORPHANAGE,
		BACKGROUND_OUR_TRAIN,
		BACKGROUND_PLAINS,
		BACKGROUND_PRIMEVAL,
		BACKGROUND_SPRING,
		BACKGROUND_STARS,
		BACKGROUND_STORM,
		BACKGROUND_SWAMP,
		BACKGROUND_THALIA_STONE,
		BACKGROUND_TO_BE_CONTINUED,
		BACKGROUND_TRAIN_CAVE,
		BACKGROUND_TRAIN_STATION,
		BACKGROUND_WINFIELD_ROOM,
		BACKGROUND_WINTER,
		BACKGROUND_MAX
	};

	// these are the sprite images, they should be ???x160 png
	// enums can be in any order, i alphabetize them for convenience while creating distributions
	// when adding a sprite you must modify the methods loadSprites, getSpriteOffsetFromText, and getSpriteTextFromOffset
	enum
	{
		SPRITE_ALPO,
		SPRITE_AXE_MAN_LEFT,
		SPRITE_AXE_MAN_MIDDLE,
		SPRITE_AXE_MAN_RIGHT,
		SPRITE_BAMBI,
		SPRITE_BAND,
		SPRITE_BAR_PATRONS,
		SPRITE_BARTENDER,
		SPRITE_BAR_TOUGH,
		SPRITE_BASEBALL_BAT,
		SPRITE_BEER,
		SPRITE_BOARD,
		SPRITE_BREAKFAST,
		SPRITE_BRUCE,
		SPRITE_BUFFY,
		SPRITE_BUNNY,
		SPRITE_CAROLINE,
		SPRITE_CARPET,
		SPRITE_CAVE_JELLY,
		SPRITE_CHEF,
		SPRITE_CHIANTI,
		SPRITE_CIRCE,
		SPRITE_CLOSED_BOX,
		SPRITE_COSTAIRS,
		SPRITE_CRAB,
		SPRITE_DEAD_END,
		SPRITE_DEAD_MONIQUE,
		SPRITE_DEAD_MONIQUE_DIM,
		SPRITE_DEAD_MONIQUE_MID,
		SPRITE_DON,
		SPRITE_DUNCAN,
		SPRITE_EAR,
		SPRITE_ELOPE_COUPLE,
		SPRITE_ENRICO,
		SPRITE_EVELYN,
		SPRITE_FEDERAL_MEN,
		SPRITE_FLABERSHAM,
		SPRITE_FLOCK,
		SPRITE_FONDUE,
		SPRITE_FUNERAL_LEFT,
		SPRITE_FUNERAL_RIGHT,
		SPRITE_GIUSEPPE,
		SPRITE_GLOVES,
		SPRITE_GUIDO,
		SPRITE_GUN,
		SPRITE_HANDSHAKE,
		SPRITE_HEART_OF_ALGEA,
		SPRITE_HEMLOCK_LEFT,
		SPRITE_HEMLOCK_RIGHT,
		SPRITE_HOTEL_BELL,
		SPRITE_JACOB,
		SPRITE_KILOWATSON,
		SPRITE_LEEDS,
		SPRITE_LIGHT_BULB,
		SPRITE_LITTLE_MARY,
		SPRITE_LOU,
		SPRITE_LOU_GUN,
		SPRITE_MARY,
		SPRITE_MARY_STANDING,
		SPRITE_MCFABISH,
		SPRITE_MEAL,
		SPRITE_MEAT_MEAL,
		SPRITE_MONA_LEFT,
		SPRITE_MONA_RIGHT,
		SPRITE_MONEY,
		SPRITE_MONICA,
		SPRITE_MONIQUE,
		SPRITE_NEWSPAPER,
		SPRITE_NOTE,
		SPRITE_OPENED_BOX,
		SPRITE_PHONE,
		SPRITE_PHOTO,
		SPRITE_POLICEMAN,
		SPRITE_POLICEWOMAN,
		SPRITE_POOL_TABLE,
		SPRITE_PORKCHOP,
		SPRITE_PORTER,
		SPRITE_PROFESSOR,
		SPRITE_RAVEN_ON_RED,
		SPRITE_ROBOT,
		SPRITE_ROCKET,
		SPRITE_ROSE,
		SPRITE_SCALES,
		SPRITE_SCYTHE,
		SPRITE_SECRETARY_1,
		SPRITE_SECRETARY_2,
		SPRITE_SEYMOUR,
		SPRITE_SHERIFF,
		SPRITE_SNAKE,
		SPRITE_SOLDIER,
		SPRITE_STOOL,
		SPRITE_TAB,
		SPRITE_TANYA_LEFT,
		SPRITE_TANYA_RIGHT,
		SPRITE_TIME_MACHINE,
		SPRITE_TONY,
		SPRITE_TRANSPARENT,
		SPRITE_TREX,
		SPRITE_TUTORS_LEFT,
		SPRITE_TUTORS_RIGHT,
		SPRITE_UZI,
		SPRITE_VEGETABLES,
		SPRITE_VERA,
		SPRITE_WATCHED,
		SPRITE_WINFIELD,
		SPRITE_WOODEN_CHAIR,
		SPRITE_MAX
	};

	// these are the movies, they should be XVID, MP3, 640x480
	// enums can be in any order, i alphabetize them for convenience while creating distributions
	// when adding a movie you must modify the methods loadMovies and getMovieOffsetFromText
	enum
	{
		MOVIE_BLESSING,
		MOVIE_BOOGIE,
		MOVIE_BOOGIE_2,
		MOVIE_BOOGIE_3,
		MOVIE_BOOGIE_4,
		MOVIE_BRUCE_ESCAPE,
		MOVIE_CALLING_POLICE,
		MOVIE_CAT_JAZZ,
		MOVIE_CAVE_JELLY,
		MOVIE_CHARIOT,
		MOVIE_COME_DOWN_STAIRS,
		MOVIE_COME_IN_LISTEN,
		MOVIE_COUPLE_HUG,
		MOVIE_CRAB_ATTACK,
		MOVIE_CRAZY,
		MOVIE_CREEPING_SHADOW,
		MOVIE_CURRY,
		MOVIE_DARKMAN,
		MOVIE_DECENTLY,
		MOVIE_DON_FINDS_BLOOD,
		MOVIE_DON_GET_GUN,
		MOVIE_DON_STARE,
		MOVIE_DON_THOUGHT,
		MOVIE_DONT_REMEMBER,
		MOVIE_DONT_WORRY,
		MOVIE_DOOR_GUNMAN,
		MOVIE_DOUBLECROSS,
		MOVIE_DRAMATIC,
		MOVIE_DRIVE_RAIN,
		MOVIE_ELOPE,
		MOVIE_ENTER_HOTEL,
		MOVIE_ESCAPE_CHAIR,
		MOVIE_EVELYN_CRYING,
		MOVIE_FACE_RAP,
		MOVIE_FIND_COSTAIRS,
		MOVIE_FLASHBACK,
		MOVIE_GLASS_OF_WHISKEY,
		MOVIE_GOOD_NIGHT,
		MOVIE_GRAB_GUIDO,
		MOVIE_GUARD_IT,
		MOVIE_GUESS_NOT,
		MOVIE_HAD_IT_COMING,
		MOVIE_HANDS_UP,
		MOVIE_HIDE_BEHIND_BOXES,
		MOVIE_HOLD_YOU,
		MOVIE_HOTEL_MURDER_SCENE,
		MOVIE_HOTEL_MURDER_SCENE_2,
		MOVIE_HOTEL_MURDER_SCENE_3,
		MOVIE_I_AM,
		MOVIE_I_SEE,
		MOVIE_IM_FREE,
		MOVIE_IN_CELLER,
		MOVIE_KILOWATSON_LATE,
		MOVIE_KNEW_LEEDS,
		MOVIE_KNOCKED_WIND,
		MOVIE_LEEDS_ANGRY,
		MOVIE_LEEDS_UPSET,
		MOVIE_LET_ME_OUT,
		MOVIE_LIGHT_SMOKE,
		MOVIE_LISTEN_AT_DOOR,
		MOVIE_LOCKED_DOOR,
		MOVIE_LONG_TIME_AGO,
		MOVIE_LOOK_AROUND,
		MOVIE_LOOK_CORNER,
		MOVIE_LOU_DON,
		MOVIE_LOVE_ANY_MORE,
		MOVIE_LURK_IN_CELLER,
		MOVIE_MAKE_BREAK,
		MOVIE_MARGARET_LOOK,
		MOVIE_MCFABISH_ARRIVES,
		MOVIE_MCFABISH_KNOCK,
		MOVIE_MEANING_OF_THIS,
		MOVIE_MEANT_IT,
		MOVIE_MEET_SCHOMES,
		MOVIE_MINIONS,
		MOVIE_MORSE,
		MOVIE_MURDER_SCENE,
		MOVIE_NOT_A_MURDERER,
		MOVIE_NOTHING_TO_CONFESS,
		MOVIE_OFFICE_FRONT,
		MOVIE_PARTNERS,
		MOVIE_PROFESSOR_LOOK,
		MOVIE_PROTECTED_SELF,
		MOVIE_RAIN_GUN,
		MOVIE_RIGHT_AHEAD,
		MOVIE_ROUGH_HIM_UP,
		MOVIE_SEE_JEWEL,
		MOVIE_SHERIFF_COMES_IN,
		MOVIE_SHERIFF_ENTERS_LOBBY,
		MOVIE_SHERIFF_IN_RAIN,
		MOVIE_SHUT_THE_DOOR,
		MOVIE_SOMETHING_WRONG,
		MOVIE_SPACESHIP_MOVIE,
		MOVIE_TIDDLY,
		MOVIE_TIED_UP,
		MOVIE_TIMELY_ASSIST,
		MOVIE_TRAIN_LEAVING,
		MOVIE_TRAIN_STATION,
		MOVIE_TRAIN_STOP,
		MOVIE_TRAIN_TRAVEL,
		MOVIE_TREX_MOVIE,
		MOVIE_TRICK,
		MOVIE_TRY_DOOR,
		MOVIE_VERA_DINING,
		MOVIE_VERA_LOOK,
		MOVIE_VERA_WALKING,
		MOVIE_VERA_WATCHING,
		MOVIE_WHO_I_MARRY,
		MOVIE_WHY_TRAIN,
		MOVIE_WINDOW_FACE,
		MOVIE_YOU_COULDNT_HAVE,
		MOVIE_MAX
	};

	// these are the sounds, they should be MP3
	// enums can be in any order, i alphabetize them for convenience while creating distributions
	// when adding a sound you must modify the methods loadSounds and getSoundOffsetFromText
	enum
	{
		SOUND_AMAZING_GRACE,
		SOUND_BREATH,
		SOUND_BROKEN_PHONE,
		SOUND_BUZZER,
		SOUND_CARD_FLIP,
		SOUND_CASH_REGISTER,
		SOUND_CHORD,
		SOUND_CLEAR_THROAT,
		SOUND_COMMOTION,
		SOUND_CRICKETS,
		SOUND_CROW,
		SOUND_CROWD_LAUGH,
		SOUND_CROWD_TALKING,
		SOUND_DIAL_TONE,
		SOUND_DINER_EATING,
		SOUND_DINER_SONG,
		SOUND_DIRK_LAUGH,
		SOUND_DIRK_SCREAM,
		SOUND_DOOR_KNOCK,
		SOUND_DRINKING_SONG,
		SOUND_EVELYN_CRY,
		SOUND_EVELYN_FOOL_SONG,
		SOUND_FLOOR_CREAK,
		SOUND_FOOTSTEPS,
		SOUND_GHOST,
		SOUND_GUN_COCK,
		SOUND_GUNSHOT,
		SOUND_HISS,
		SOUND_HOTEL_BELL,
		SOUND_HOTEL_DINNER_SONG,
		SOUND_HOWL,
		SOUND_IDEA,
		SOUND_JACOB_SONG,
		SOUND_JAIL_DOOR,
		SOUND_KEY_CLICK,
		SOUND_LITTLE_MARY,
		SOUND_LITTLE_MARY_2,
		SOUND_MACHINE_GUN,
		SOUND_MARY_HAS_BLUES_SONG,
		SOUND_MARY_HOTEL_DOOR,
		SOUND_MARY_LAUGH,
		SOUND_MARY_RESOURCEFUL,
		SOUND_MARY_SCREAM,
		SOUND_MARY_STRONG_SONG,
		SOUND_MARY_WEAK_SONG,
		SOUND_MARY_YAWN,
		SOUND_MELANCHOLY,
		SOUND_MONA_LAUGH,
		SOUND_MONA_SONG,
		SOUND_MOTHERS_GRAVE_SONG,
		SOUND_MOTHERS_GRAVE_SONG3,
		SOUND_MOTHERS_GRAVE_SONG4,
		SOUND_OUTDOOR,
		SOUND_PAPER_RUSTLE,
		SOUND_PHONE_RING,
		SOUND_PHONE_TONE,
		SOUND_POLICE_SIREN,
		SOUND_POP_CORK,
		SOUND_PORKCHOP_DROP,
		SOUND_POURING_BEER,
		SOUND_PUNCH_LINE,
		SOUND_RAIN,
		SOUND_SEASHORE,
		SOUND_SHERIFF_SUE_SONG,
		SOUND_SIGH,
		SOUND_STARLINGS,
		SOUND_STRIPPER_DRUM,
		SOUND_STRIPPER_SONG,
		SOUND_SWEDISH_ROCKABILLY,
		SOUND_TADA,
		SOUND_TELL_MARY_SONG,
		SOUND_THUNDER,
		SOUND_TRAFFIC,
		SOUND_TRAIN_ROCK,
		SOUND_TRAIN_SONG_1,
		SOUND_TRAIN_SONG_2,
		SOUND_TRAIN_SONG_3,
		SOUND_TRAIN_SONG_4,
		SOUND_TRAIN_SONG_5,
		SOUND_TRAIN_SONG_6,
		SOUND_TRAIN_SOUND,
		SOUND_TROUBLE_SONG,
		SOUND_TYPEWRITER,
		SOUND_UNDERESTIMATE,
		SOUND_WIND,
		SOUND_WOMAN_CRYING,
		SOUND_WOMAN_SIGH,
		SOUND_WRITING,
		SOUND_MAX
	};

	// these are the scenarios
	// enums must maintain their current order for saved games and skip memory
	// I know, pure evil to write an enum to disk but i couldn't pass up the convenience
	// a new scenario should be added last before SCENARIO_MAX
	// when adding a scenario you must modify the methods loadScenarios and getScenarioOffsetFromText
	enum
	{
		SCENARIO_START,
		SCENARIO_DRINK_AT_SAMS_AFTER_MURDER,
		SCENARIO_TELL_MARY_AFTER_MURDER,
		SCENARIO_QUIT_AFTER_TONY,
		SCENARIO_SEARCH_HOUSE_AFTER_TONY,
		SCENARIO_CRUSTACEAN_COVE,
		SCENARIO_HAUNTED_CAVES,
		SCENARIO_LOU_DIAMOND,
		SCENARIO_CRUSTACEAN_COVE_CHOICES,
		SCENARIO_BAR_QUIT,
		SCENARIO_BRUCE,
		SCENARIO_QUESTION_DON,
		SCENARIO_CRUSTACEAN_COVE_CONTINUE,
		SCENARIO_CRUSTACEAN_COVE_KILLER_CHOICES,
		SCENARIO_CRUSTACEAN_COVE_ENRICO_KILLER,
		SCENARIO_CRUSTACEAN_COVE_GIUSEPPE_KILLER,
		SCENARIO_CRUSTACEAN_COVE_BRUCE_KILLER,
		SCENARIO_CRUSTACEAN_COVE_EVELYN_KILLER,
		SCENARIO_CRUSTACEAN_COVE_DON_KILLER,
		SCENARIO_CRUSTACEAN_COVE_LOU_KILLER,
		SCENARIO_CRUSTACEAN_COVE_GUIDO_KILLER,
		SCENARIO_OHANNITY_AFTER_CRUSTACEAN_COVE,
		SCENARIO_MONA,
		SCENARIO_MONA_CHOOSE,
		SCENARIO_MONA_CONTINUE,
		SCENARIO_TRAIN_SUSPECT_CHOOSE,
		SCENARIO_TRAIN_CAVE,
		SCENARIO_TRAIN_JACOB,
		SCENARIO_COLLAGE_GIRLS,
		SCENARIO_MONICA,
		SCENARIO_VERA,
		SCENARIO_SCHOMES_KILOWATSON,
		SCENARIO_DUNCAN,
		SCENARIO_KILBANE,
		SCENARIO_MCFABISH,
		SCENARIO_LADY_MARGARET,
		SCENARIO_TRAIN_CONTINUE,
		SCENARIO_TRAIN_GUILTY_CHOOSE,
		SCENARIO_HEMLOCK_GUILTY,
		SCENARIO_KILOWATSON_GUILTY,
		SCENARIO_COLLAGE_GIRLS_GUILTY,
		SCENARIO_DUNCAN_GUILTY,
		SCENARIO_MARGARET_GUILTY,
		SCENARIO_MCFABISH_GUILTY,
		SCENARIO_MONICA_GUILTY,
		SCENARIO_VERA_GUILTY,
		SCENARIO_KILBANE_GUILTY,
		SCENARIO_JACOB_GUILTY,
		SCENARIO_PORTER_GUILTY,
		SCENARIO_DIRK_TRIVIA,
		SCENARIO_LEEDS_OFFICE_TRIVIA,
		SCENARIO_LEED_TRIVIA,
		SCENARIO_FUNERAL_TRIVIA,
		SCENARIO_DON_TRIVIA,
		SCENARIO_TRAIN_BABY_TRIVIA,
		SCENARIO_LOU_TRIVIA,
		SCENARIO_MONEY_TRIVIA,
		SCENARIO_SHERIFF_TRIVIA,
		SCENARIO_CIRCE_TRIVIA,
		SCENARIO_TERROR_TRIVIA,
		SCENARIO_VEAL_TRIVIA,
		SCENARIO_MARGARET_TRIVIA,
		SCENARIO_BUNNY_TRIVIA,
		SCENARIO_BRUCE_TRIVIA,
		SCENARIO_CAROLINE_TRIVIA,
		SCENARIO_MONIQUE_TRIVIA,
		SCENARIO_LOCH_TRIVIA,
		SCENARIO_REMEMBER_PAST_CHOICES,
		SCENARIO_SEARCH_LEEDS_HOUSE,
		SCENARIO_PAST_SEYMOUR,
		SCENARIO_MONIQUE_DEAD,
		SCENARIO_THE_STRIPPER_TRIVIA,
		SCENARIO_REMEMBER_PAST_CONTINUE,
		SCENARIO_HOOVER_TRIVIA,
		SCENARIO_DON_LOUISIANA_TRIVIA,
		SCENARIO_MAX
	};

	enum
	{
		MODE_LOAD,
		MODE_SAVE
	};

	MainWindow(QWidget *parent = 0);
	~MainWindow();

	bool init(QApplication *app);
	int getCurrentSceneType();
	bool getConversationComplete();
	void setAccelerateConversation();
	void unsetSkipping();
	bool isSkipping();
#ifdef CONVERSATION_KEY_CLICK
	bool getConversationPlayEstablished();
#endif
	bool getSoundPlayEstablished();
	bool getMoviePlayEstablished();
	bool isAudioVideoWarnDisplayed();
	bool isThreadQuit();
	void accelerateSound();
	void accelerateMovie();
	void previousScene();
	bool getSpriteFileString(int index,string &fileName) const;
private:
	bool loadBackgrounds(const string &dataDirectory);
	bool loadSprites(const string &dataDirectory);
	bool loadMovies(const string &dataDirectory);
	bool loadScenarios(const string &dataDirectory);
	bool parseScenario(const string &currentFile,GameScenario &gameScenario);
	bool parseSprite(QXmlStreamReader &xml,GameScenario &gameScenario);
	int getBackgroundOffsetFromText(const string &backgroundText);
	bool parseSpriteInfo(QXmlStreamReader &xml,GameScene &gameScene);
	int getSpriteOffsetFromText(const string &spriteName);
	int getVerticalPositionFromText(const string &verticalString);
	int getHorizontalPositionFromText(const string &horizontalString);
	int getDirectionFromText(const string &directionString);
	bool parseConversation(QXmlStreamReader &xml,GameScenario &gameScenario);
	void performScene();
	void performConversation();
	void performSprite();
	void performTrivia();
	void performTriviaReturn();
	bool parseMovie(QXmlStreamReader &xml,GameScenario &gameScenario);
	int getMovieOffsetFromText(const string &movieText);
	bool parseMovieSubtitle(QXmlStreamReader &xml,GameScene &gameScene);
	void performMovie();
	bool parseUserChoice(QXmlStreamReader &xml,GameScenario &gameScenario);
	bool parseChoice(QXmlStreamReader &xml,GameScene &gameScene);
	int getScenarioOffsetFromText(const string &scenarioName);
	void performUserChoice();
	bool loadSounds(const string &dataDirectory);
	bool parseSound(QXmlStreamReader &xml,GameScenario &gameScenario);
	int getSoundOffsetFromText(const string &soundName);
	void performSound();
	void enableGamePlayButtons(bool enabled);
	void enableTitleButton();
	bool parseStartScenario(QXmlStreamReader &xml,GameScenario &gameScenario);
	void performStartScenario();
	bool parseGameOver(QXmlStreamReader &xml,GameScenario &gameScenario);
	void performGameOver();
	void showLoadGame();
	void enablePreviousNext();
	void setSaveLoadIcons();
	bool getBackgroundTextFromOffset(int offset,string &text);
	bool getSpriteTextFromOffset(int offset,string &text);
	bool crossCheckBackgroundSprite();
	void saveLoadButtonAction(int offset);
	void loadSaveIcons();
	bool createSaveNodeIcon(int offset,const SaveNode &saveNode);
	bool isPreviousAvailable(int &previousScene);
	bool parseTrivia(QXmlStreamReader &xml,GameScenario &gameScenario);
	bool parseTriviaReturn(QXmlStreamReader &xml,GameScenario &gameScenario);

	QWidget *m_centralWidget;

	QStackedLayout *m_mainStack;

	// game play widgets and layouts
	QWidget *m_gameCentralWidget;
	QPushButton *m_saveButton;
	QPushButton *m_loadButton;
	QPushButton *m_skipButton;
	QPushButton *m_titleButton;
	QPushButton *m_previousButton;
	QPushButton *m_nextButton;
	//Phonon::VideoWidget *m_videoWidget;
	GamePlayVideoWidget *m_videoWidget;
	GamePlayLabel *m_pixmapDisplayLabel;
	GamePlayLabelWithKeyboardGrab *m_conversationLabel;

	QHBoxLayout *m_horizontalButtonLayout;
	QHBoxLayout *m_imageMovieHorizontalLayout;
	QVBoxLayout *m_gamePlayVerticalLayout;

	// title widgets and layouts
	QWidget *m_titleCentralWidget;
	QLabel *m_titleLabel;
	QPushButton *m_gamePlayButton;
	QPushButton *m_gameLoadButton;
	QPushButton *m_instructionsButton;
	QPushButton *m_aboutButton;
	QPushButton *m_problemsWithAudioVideoButton;
	QPushButton *m_aboutQtButton;
	QPushButton *m_quitButton;
	QFrame *m_titleTopLine;
	QLabel *m_titlePixmapDisplayLabel;
	QFrame *m_titleBottomLine;

	QVBoxLayout *m_titleVerticalLayout;
	QHBoxLayout *m_titleHorizontalLayout;
	QHBoxLayout *m_horizontalTitleLabelLayout;
	QHBoxLayout *m_horizontalGamePlayButtonLayout;
	QHBoxLayout *m_horizontalGameLoadButtonLayout;
	QHBoxLayout *m_horizontalInstructionsButtonLayout;
	QHBoxLayout *m_horizontalAboutButtonLayout;
	QHBoxLayout *m_horizontalproblemsWithAudioVideoButtonLayout;
	QHBoxLayout *m_horizontalAboutQtButtonLayout;
	QHBoxLayout *m_horizontalQuitButtonLayout;
	QHBoxLayout *m_horizontalTitlePixmapDisplayLabelLayout;

	// save - load widgets and layouts
	QWidget *m_saveLoadCentralWidget;
	QLabel *m_saveLoadTitleLabel;
	QPushButton *m_saveLoadPreviousButton;
	QPushButton *m_saveLoadNextButton;
	QPushButton *m_saveLoad1Button;
	QLabel *m_saveLoad1Label;
	QPushButton *m_saveLoad2Button;
	QLabel *m_saveLoad2Label;
	QPushButton *m_saveLoad3Button;
	QLabel *m_saveLoad3Label;
	QPushButton *m_saveLoad4Button;
	QLabel *m_saveLoad4Label;
	QPushButton *m_saveLoad5Button;
	QLabel *m_saveLoad5Label;
	QPushButton *m_saveLoad6Button;
	QLabel *m_saveLoad6Label;
	QPushButton *m_saveLoadCancelButton;

	QVBoxLayout *m_saveLoadVerticalLayout;
	QHBoxLayout *m_saveLoadTitleHorizontalLayout;
	QHBoxLayout *m_saveLoadPreviousNextHorizontalLayout;
	QHBoxLayout *m_saveLoad1HorizontalLayout;
	QHBoxLayout *m_saveLoad2HorizontalLayout;
	QHBoxLayout *m_saveLoadCancelHorizontalLayout;
	QHBoxLayout *m_saveLoadButton1HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel1HorizontalLayout;
	QVBoxLayout *m_saveLoadButton1VerticalLayout;
	QHBoxLayout *m_saveLoadButton2HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel2HorizontalLayout;
	QVBoxLayout *m_saveLoadButton2VerticalLayout;
	QHBoxLayout *m_saveLoadButton3HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel3HorizontalLayout;
	QVBoxLayout *m_saveLoadButton3VerticalLayout;
	QHBoxLayout *m_saveLoadButton4HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel4HorizontalLayout;
	QVBoxLayout *m_saveLoadButton4VerticalLayout;
	QHBoxLayout *m_saveLoadButton5HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel5HorizontalLayout;
	QVBoxLayout *m_saveLoadButton5VerticalLayout;
	QHBoxLayout *m_saveLoadButton6HorizontalLayout;
	QHBoxLayout *m_saveLoadLabel6HorizontalLayout;
	QVBoxLayout *m_saveLoadButton6VerticalLayout;

	// about widgets and layouts
	QWidget *m_aboutCentralWidget;
	QLabel *m_aboutIconPixmapLabel;
	QLabel *m_aboutTitleLabel;
	QLabel *m_aboutVersionLabel;
	QLabel *m_copyrightLabel;
	QTextBrowser *m_aboutTextBrowser;
	QPushButton *m_aboutTitleButton;

	QVBoxLayout *m_aboutVerticalLayout;
	QHBoxLayout *m_horizontalAboutIconPixmapLayout;
	QHBoxLayout *m_horizontalAboutTitleLayout;
	QHBoxLayout *m_horizontalAboutVersionLayout;
	QHBoxLayout *m_horizontalAboutCopyrightLayout;
	QHBoxLayout *m_horizontalAboutTitleButtonLayout;

	// instructions widgets and layouts
	QWidget *m_instructionsCentralWidget;
	QTextBrowser *m_instructionsTextBrowser;
	QPushButton *m_instructionsTitleButton;

	QVBoxLayout *m_instructionsVerticalLayout;
	QHBoxLayout *m_horizontalInstructionsTitleButtonLayout;


	Phonon::MediaObject *m_movieMediaObject;
	Phonon::AudioOutput *m_movieAudioOutput;

	Phonon::MediaObject *m_soundEffectMediaObject;
	Phonon::AudioOutput *m_soundEffectAudioOutput;

// I'm having too many problems with a gstreamer delay, I'll disable the key click in Linux for now
#ifdef CONVERSATION_KEY_CLICK
	Phonon::MediaObject *m_conversationTypingMediaObject;
	Phonon::AudioOutput *m_conversationTypingAudioOutput;
#endif

	QPixmap m_currentBackgroundPixmap;

	QTimer *m_spriteSlideTimer;
	QTimer *m_conversationWriteTimer;
	QTimer *m_conversationWriteSkipTimer;

	vector<QPixmap> m_slideVector;
	int m_currentVectorImage;

	string m_currentConversationString;
	int m_currentConversationOffset;
	bool m_conversationComplete;
	bool m_accelerateConversation;

	vector<string> m_backgroundVector;
	vector<string> m_spriteVector;
	vector<string> m_movieVector;
	vector<string> m_soundVector;

	GameScenario m_currentGameScenario;
	vector<GameScenario> m_gameScenarioVector;

	int m_currentSceneType;

	vector<SceneMovieSubtitleNodeInUse> m_sceneMovieSubtitleNodeInUseVector;

	bool m_soundEffectStopStateFound;

	SkipContainer m_skipContainer;
	SaveLoadContainer m_saveLoadContainer;

	int m_currentGameScenarioOffset;
	int m_currentGameSceneOffset;

	bool m_skipping;

	vector<QIcon> m_saveLoadIconVector;

	int m_previousPage;

	int m_currentSaveLoadPage;

	int m_currentSaveLoadMode;

	string m_aboutText;
	string m_instructionsText;
	string m_problemsWithAudioVideoText;

#ifdef CONVERSATION_KEY_CLICK
	AudioVideoMonitorThread m_audioVideoMonitorThreadConversation;
#endif
	AudioVideoMonitorThread m_audioVideoMonitorThreadSound;
	AudioVideoMonitorThread m_audioVideoMonitorThreadMovie;
	QMutex m_audioVideoMonitorThreadMutex;

#ifdef CONVERSATION_KEY_CLICK
	bool m_conversationPlayEstablished;
#endif
	bool m_soundPlayEstablished;
	bool m_moviePlayEstablished;

	bool m_audioVideoWarnDisplayed;

	bool m_threadQuit;

	string m_previousPlayEstablishedFileName;
	bool previousPlayEstablished;

	int m_triviaReturnScenarioOffset;
	int m_triviaReturnSceneOffset;
	QPixmap m_triviaPixMap;
	QPixmap m_triviaBackgroundPixmap;
signals:
	void signalAdvanceScenario();
	void signalStartScenario(int scenarioOffset);
	void signalRewindScenario();
private slots:
	void titleButtonClicked();
	void movieMediaStateChanged(Phonon::State newstate,Phonon::State oldstate);
	void slideSprite();
	void gamePlayButtonClicked();
	void startScenario(int scenarioOffset);
	void advanceScenario();
	void movieRunTime(qint64 time);
#ifdef LINUX_BUILD
	void soundEffectRunTime(qint64);
#endif
#if defined(CONVERSATION_KEY_CLICK) && defined(LINUX_BUILD)
	void conversationTypingRunTime(qint64);
#endif
	void rewindScenario();
	void writeConversation();
	void writeSkipConversation();
	void soundEffectMediaStateChanged(Phonon::State newstate,Phonon::State oldstate);
#ifdef CONVERSATION_KEY_CLICK
	void conversationTypingMediaStateChanged(Phonon::State newstate,Phonon::State);
#endif
	void skipButtonClicked();
	void gameLoadButtonClicked();
	void saveLoadCancelButtonClicked();
	void loadButtonClicked();
	void saveButtonClicked();
	void saveLoadPreviousButtonClicked();
	void saveLoadNextButtonClicked();
	void saveLoad1ButtonClicked();
	void saveLoad2ButtonClicked();
	void saveLoad3ButtonClicked();
	void saveLoad4ButtonClicked();
	void saveLoad5ButtonClicked();
	void saveLoad6ButtonClicked();
	void aboutButtonClicked();
	void aboutTitleButtonClicked();
	void instructionsButtonClicked();
	void instructionsTitleButtonClicked();
	void problemsWithAudioVideoButtonClicked();
	void aboutQtButtonClicked();
	void audioVideoWarn();
	void previousButtonClicked();
	void nextButtonClicked();
};

#endif // MAINWINDOW_H
