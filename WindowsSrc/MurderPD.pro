# -------------------------------------------------
# Project created by QtCreator 2011-01-11T00:39:36
# -------------------------------------------------
QT += phonon
TARGET = MurderPD
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    activesprite.cpp \
    gameplaylabel.cpp \
    gamescenario.cpp \
    gamescene.cpp \
    userchoicedialog.cpp \
    skipcontainer.cpp \
    saveloadcontainer.cpp \
    saveloadverificationdialog.cpp \
    audiovideomonitorthread.cpp \
    gameplayvideowidget.cpp \
    triviadialog.cpp
HEADERS += mainwindow.h \
    activesprite.h \
    gameplaylabel.h \
    gamescenario.h \
    gamescene.h \
    userchoicedialog.h \
    builddefs.h \
    skipcontainer.h \
    saveloadcontainer.h \
    saveloadverificationdialog.h \
    linuxconfig.h \
    audiovideomonitorthread.h \
    gameplayvideowidget.h \
    triviadialog.h

RC_FILE = MurderPD.rc

