/*
 * audiovideomonitorthread.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef AUDIOVIDEOMONITORTHREAD_H
#define AUDIOVIDEOMONITORTHREAD_H

#include <QtCore/QThread>
#include <time.h>

class MainWindow;

#define ESTABLISH_PLAY_TIMEOUT							10

class AudioVideoMonitorThread : public QThread
{
	Q_OBJECT
public:
	enum
	{
		AUDIO_VIDEO_CONVERSATION,
		AUDIO_VIDEO_SOUND,
		AUDIO_VIDEO_MOVIE
	};

	AudioVideoMonitorThread();

	void setParentAndType(MainWindow *parent,int audioVideoType);
	void run();
private:
	int m_audioVideoType;
	MainWindow *m_parent;

	bool m_monitoring;
	time_t m_startTime;
signals:
	void signalAdvanceScenario();
	void signalAudioVideoWarn();
};

#endif // AUDIOVIDEOMONITORTHREAD_H
