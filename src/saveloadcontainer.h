/*
 * saveloadcontainer.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SAVELOADCONTAINER_H
#define SAVELOADCONTAINER_H

#include <QtCore/QFile>
#include <vector>
#include <string>
#include <stdio.h>
#include <time.h>

using namespace std;

#define TOTAL_GAME_SAVES			24

class SaveSpriteNode
{
public:
	SaveSpriteNode();
	SaveSpriteNode(int verticalPosition,int horizontalPosition,const string &spriteName);
	SaveSpriteNode(const SaveSpriteNode &ssn);
	SaveSpriteNode &operator = (const SaveSpriteNode &ssn);

	int getVerticalPosition() const;
	int getHorizontalPosition() const;
	const string &getSpriteName() const;
private:
	int m_verticalPosition;
	int m_horizontalPosition;
	string m_spriteName;
};

class SaveNode
{
public:
	SaveNode();
	SaveNode(time_t currentTime,int scenarioOffset,int sceneOffset,const string &backgroundName,const vector<SaveSpriteNode> &saveSpriteNodeVector);
	SaveNode(const SaveNode &sn);
	SaveNode &operator = (const SaveNode &sn);

	bool getInUse() const;
	time_t getTime() const;
	int getScenarioOffset() const;
	int getSceneOffset() const;
	const string &getBackgroundName() const;
	const vector<SaveSpriteNode> &getSaveSpriteNodeVector() const;
private:
	bool m_inUse;
	time_t m_time;
	int m_scenarioOffset;
	int m_sceneOffset;
	string m_backgroundName;
	vector<SaveSpriteNode> m_saveSpriteNodeVector;
};

class SaveLoadContainer
{
public:
    SaveLoadContainer();
private:
	SaveLoadContainer(const SaveLoadContainer&) {;}
	SaveLoadContainer &operator = (const SaveLoadContainer&) {return *this;}
public:
	bool init(const string &homeDirectory);

	time_t getSaveNodeTime(int index);
	bool getSaveNodeInUse(int index);
	bool setSaveNode(int index,const SaveNode &sn);
	bool getSaveNode(int index,SaveNode &sn);
	bool isLoadAvailable();
private:
	bool parseSaveNode(unsigned char *&dataOffset,int &length,int index);
	bool parseSaveSpriteNodes(unsigned char *&dataOffset,int &length,vector<SaveSpriteNode> &saveSpriteNodeVector);
	bool writeSaveNodesToDisk();
	void addSaveNodeToWriteVector(const SaveNode &saveNode,vector<unsigned char> &fileWriteVector);
	void addSaveSpriteNodeToWriteVector(const SaveSpriteNode &saveSpriteNode,vector<unsigned char> &fileWriteVector);

	QFile m_gameSaveFile;
	vector<SaveNode> m_saveNodeVector;
};

#endif // SAVELOADCONTAINER_H
