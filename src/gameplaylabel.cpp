/*
 * gameplaylabel.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "gameplaylabel.h"
#include "mainwindow.h"
#include <QtCore/QCoreApplication>

GamePlayLabel::GamePlayLabel(MainWindow *mainWindow,QWidget * parent /*= 0*/,Qt::WindowFlags f /*= 0*/) : QLabel(parent,f),m_mainWindow(mainWindow)
{
}

void GamePlayLabel::mousePressEvent(QMouseEvent *event)
{
	if(m_mainWindow->isSkipping())
		m_mainWindow->unsetSkipping();
	else
	{
		if(m_mainWindow->getCurrentSceneType() == GameScene::SCENE_CONVERSATION)
		{
			if(event->button() == Qt::LeftButton)
				actOnConversationClick();
			else
				QLabel::mousePressEvent(event);
		}
		else if(m_mainWindow->getCurrentSceneType() == GameScene::SCENE_SOUND)
		{
			if(event->button() == Qt::LeftButton)
				m_mainWindow->accelerateSound();
			else
				QLabel::mousePressEvent(event);
		}
		else if(m_mainWindow->getCurrentSceneType() == GameScene::SCENE_MOVIE)
		{
			if(event->button() == Qt::LeftButton)
				m_mainWindow->accelerateMovie();
			else
				QLabel::mousePressEvent(event);
		}
		else
			QLabel::mousePressEvent(event);
	}
}

void GamePlayLabel::actOnConversationClick()
{
	if(m_mainWindow->getConversationComplete())
		signalAdvanceScenario();
	else
		m_mainWindow->setAccelerateConversation();
}

MainWindow *GamePlayLabel::getMainWindow()
{
	return m_mainWindow;
}



GamePlayLabelWithKeyboardGrab::GamePlayLabelWithKeyboardGrab(MainWindow *mainWindow,QWidget *parent /*= 0*/,Qt::WindowFlags f /*= 0*/) :
		GamePlayLabel(mainWindow,parent,f),
		m_grabKeyboardEstablished(false),
		m_keyboardReleasedToChild(false)
{
}

// OK, a convoluted mess but it solved my problems for loosing keyboard input in third
// party apps that take focus and locking my app to the screen when it retakes focus
bool GamePlayLabelWithKeyboardGrab::event(QEvent *e)
{
	if(!m_keyboardReleasedToChild)
	{
		if(e->type() == QEvent::Show)
		{
			grabKeyboard();
			m_grabKeyboardEstablished = true;
		}
		if(m_grabKeyboardEstablished)
		{
			if(e->type() == QEvent::WindowActivate)
			{
				this->hide();
				this->show();
			}
		}
		if(e->type() == QEvent::Hide)
			releaseKeyboard();
		if(e->type() == QEvent::WindowDeactivate)
			releaseKeyboard();
	}
	return GamePlayLabel::event(e);
}

void GamePlayLabelWithKeyboardGrab::releaseKeyboardToChild(bool release)
{
	if(release)
	{
		m_keyboardReleasedToChild = true;
		releaseKeyboard();
	}
	else
	{
		m_keyboardReleasedToChild = false;
		grabKeyboard();
	}
}

void GamePlayLabelWithKeyboardGrab::keyPressEvent(QKeyEvent *event)
{
	if(getMainWindow()->isSkipping())
		getMainWindow()->unsetSkipping();
	else
	{
		if(getMainWindow()->getCurrentSceneType() == GameScene::SCENE_CONVERSATION)
		{
			if(event->text() == " ")
				actOnConversationClick();
			else if(event->key() == Qt::Key_Return)
				actOnConversationClick();
			else if(event->key() == Qt::Key_Enter)
				actOnConversationClick();
			else if(event->text() == "k" || event->text() == "K")
				signalSkipButton();
			else if(event->text() == "s" || event->text() == "S")
				signalSaveButton();
			else if(event->text() == "l" || event->text() == "L")
				signalLoadButton();
			else if(event->text() == "t" || event->text() == "T")
				signalTitleButton();
			else if(event->key() == Qt::Key_Backspace)
				getMainWindow()->previousScene();
			else
				GamePlayLabel::keyPressEvent(event);
		}
		else if(getMainWindow()->getCurrentSceneType() == GameScene::SCENE_SOUND)
		{
			if(event->text() == " ")
				getMainWindow()->accelerateSound();
			else if(event->key() == Qt::Key_Return)
				getMainWindow()->accelerateSound();
			else if(event->key() == Qt::Key_Enter)
				getMainWindow()->accelerateSound();
			else if(event->key() == Qt::Key_Backspace)
				getMainWindow()->previousScene();
			else
				GamePlayLabel::keyPressEvent(event);
		}
		else if(getMainWindow()->getCurrentSceneType() == GameScene::SCENE_MOVIE)
		{
			if(event->text() == " ")
				getMainWindow()->accelerateMovie();
			else if(event->key() == Qt::Key_Return)
				getMainWindow()->accelerateMovie();
			else if(event->key() == Qt::Key_Enter)
				getMainWindow()->accelerateMovie();
			else if(event->key() == Qt::Key_Backspace)
				getMainWindow()->previousScene();
			else
				GamePlayLabel::keyPressEvent(event);
		}
		else
			GamePlayLabel::keyPressEvent(event);
	}
}
