/*
 * gameplaylabel.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef GAMEPLAYLABEL_H
#define GAMEPLAYLABEL_H

#include "builddefs.h"
#include <QtGui/QLabel>
#include <QtGui/QMouseEvent>
#include <QtGui/QKeyEvent>

class MainWindow;

class GamePlayLabel : public QLabel
{
	Q_OBJECT
public:
	GamePlayLabel(MainWindow *mainWindow,QWidget *parent = 0,Qt::WindowFlags f = 0);
protected:
	virtual void mousePressEvent(QMouseEvent *event);
	void actOnConversationClick();
	MainWindow *getMainWindow();
private:
	MainWindow *m_mainWindow;
signals:
	void signalAdvanceScenario();
};


// this is the keyboard grabber, Highlander here, there can be only one!!!!
class GamePlayLabelWithKeyboardGrab : public GamePlayLabel
{
	Q_OBJECT
public:
	GamePlayLabelWithKeyboardGrab(MainWindow *mainWindow,QWidget *parent = 0,Qt::WindowFlags f = 0);

	void releaseKeyboardToChild(bool release);
protected:
	virtual bool event(QEvent *e);
	virtual void keyPressEvent(QKeyEvent *event);
private:
	bool m_grabKeyboardEstablished;
	bool m_keyboardReleasedToChild;
signals:
	void signalSaveButton();
	void signalLoadButton();
	void signalSkipButton();
	void signalTitleButton();
};

#endif // GAMEPLAYLABEL_H
