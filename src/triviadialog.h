/*
 * triviadialog.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef TRIVIADIALOG_H
#define TRIVIADIALOG_H

#include <QtGui/QDialog>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QFrame>
#include <QtGui/QButtonGroup>
#include <vector>
#include <string>

using namespace std;

class MainWindow;

#define TRIVIA_DIALOG_TITLE			"What's The Answer?"
#define MINIMUM_QUESTION_HEIGHT		124

class TriviaDialog : public QDialog
{
    Q_OBJECT
public:
	TriviaDialog(const vector<string> &choicesVector,
				 const string &question,
				 int spriteOffset,
				 QWidget *parent = 0,
				 Qt::WindowFlags f = 0);
	void init();
private:
	QVBoxLayout *m_mainVerticalLayout;

	QFrame *m_topLine;

	QHBoxLayout *m_questionHorizontalLayout;
	QLabel *m_spriteLabel;
	QLabel *m_questionLabel;

	QFrame *m_questionLine;

	QVBoxLayout *m_radioVerticalLayout;
	QButtonGroup *m_radioButtonGroup;

	QFrame *m_radioLine;

	QHBoxLayout *m_buttonHorizontalLayout;
	QPushButton *m_okButton;

	MainWindow *m_mainWindow;

	vector<string> m_choicesVector;
	string m_question;
	int m_spriteOffset;

signals:

private slots:
	void userChoiceDialogRejected();
	void userChoiceDialogAccepted();

};

#endif // TRIVIADIALOG_H
